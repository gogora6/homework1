fun main() {
    val result = task1(arrayOf(1, 2, 3, 4, 5, 6))
    println(result)
    println(CheckPalindrome("LevEl"))
}

fun task1(numbersArray: Array<Int>): Int {
    var sum = 0
    var numbersCount = 0
    for (index in numbersArray.indices) {
        if (index % 2 == 0) {
            sum += numbersArray[index]
            numbersCount++
        }
    }
    return sum / numbersCount
}

fun CheckPalindrome(text: String): Boolean {
    val lowerText = text.lowercase()
    if (lowerText == lowerText.reversed()) return true

    return false
}